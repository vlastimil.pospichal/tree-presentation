<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" encoding="UTF-8" />

<xsl:template match="/">
    <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;
</xsl:text>
    <html>
    <xsl:apply-templates />
    </html>
</xsl:template>

<xsl:template match="/root">
    <head>
    <title>Diskuzní fórum</title>
    <link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
    <xsl:apply-templates />
    </body>
</xsl:template>

<xsl:template match="div">
    <div>
    <xsl:apply-templates />
    </div>
</xsl:template>

</xsl:stylesheet>