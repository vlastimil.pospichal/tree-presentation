<?php
$db = new PDO('sqlite:tree.sqlite');
$prep = $db->prepare('SELECT id, pid, name FROM tree WHERE gid=? ORDER BY id;');
$prep->execute(array(2));
$doc = new DOMdocument();
$tree = array();
$tree[0] = $doc->appendChild(new DOMElement('root'));
while ($row = $prep->fetch(PDO::FETCH_ASSOC)) {
    $id = $row['id'];
    $pid = isset($row['pid']) ? $row['pid'] : 0;
    $pid = isset($tree[$pid]) ? $pid : 0;
    $tree[$id] = $tree[$pid]->appendChild(new DOMElement('div', htmlspecialchars($row['name'])));
}
$xsl = new DOMdocument();
$xsl->load('tree.xsl');
$xslt = new XSLTProcessor();
$xslt->importStylesheet($xsl);
echo $xslt->transformToXML($doc);
